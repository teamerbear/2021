import os, sys
import numpy as np


# The rates for test_1.txt are:
#   Epsilon: 22 (10110)
#   Gamma:    9 (01001)
#   Power consumption: 198
#
#   Oxygen Generator: 23 (10111)
#   CO2 Scrubber:     10 (01010)
#   Life Support:     230
#
# The rates for test_2.txt are:
#   Epsilon: 3938 (111101100010)
#   Gamma:    157 (000010011101)
#   Power consumption: 618266
#
#   Oxygen Generator: 4091 (111111111011)
#   CO2 Scrubber:      237 (000011101101)
#   Life Support:   969567
#
# The rates for input.txt are:
#   Epsilon: 419
#   Gamma:  3676
#   Power consumption: 1540244
#
#   Oxygen Generator: 1161
#   CO2 Scrubber:     3621
#   Life Support:  4203981


def str_to_digits(num_str):
  return np.array([int(i) for i in list(num_str)])


def digits_to_num(digits):
  num = 0
  for i in range(0, digits.shape[0]):
    shift = digits.shape[0] - 1 - i
    num |= (digits[i] << shift)
  
  return num


def get_nums_with_bit_in_position(matrix, bit, pos):
  pruned = np.zeros((1, matrix.shape[1]), dtype=np.int64)

  found_first = False
  for i in range(0, matrix.shape[0]):
    if matrix[i, pos] == bit:
      if not found_first:
        pruned[0, :] = matrix[i, :]
        found_first = True
      else:
        pruned = np.vstack([pruned, matrix[i, :]])
  
  return pruned


def reduce_bit_criteria_most_common(matrix, position):
  if matrix.shape[0] == 1:
    return matrix

  odd = (matrix.shape[0] % 2 == 1)
  half = int(matrix.shape[0] / 2)
  sums = np.sum(matrix, axis=0, dtype=np.int64)
  pruned = None
  if (not odd) and (sums[position] == half):
    # Tie goes to 1 in this case.
    pruned = get_nums_with_bit_in_position(matrix, 1, position)
  elif sums[position] > half:
    # This means a 1 was the most common digit
    pruned = get_nums_with_bit_in_position(matrix, 1, position)
  else:
    # This means a 0 was the most common digit
    pruned = get_nums_with_bit_in_position(matrix, 0, position)

  return reduce_bit_criteria_most_common(pruned, position + 1)


def reduce_bit_criteria_least_common(matrix, position):
  if matrix.shape[0] == 1:
    return matrix

  odd = (matrix.shape[0] % 2 == 1)
  half = int(matrix.shape[0] / 2)
  sums = np.sum(matrix, axis=0, dtype=np.int64)
  pruned = None
  if (not odd) and (sums[position] == half):
    # Tie goes to 0 in this case.
    pruned = get_nums_with_bit_in_position(matrix, 0, position)
  elif sums[position] > half:
    # This means a 1 was the most common digit (0 is least common)
    pruned = get_nums_with_bit_in_position(matrix, 0, position)
  else:
    # This means a 0 was the most common digit (1 is least common)
    pruned = get_nums_with_bit_in_position(matrix, 1, position)

  return reduce_bit_criteria_least_common(pruned, position + 1)


if __name__ == "__main__":
  if len(sys.argv) < 2:
    raise SyntaxError("USAGE: python3 puzzle.py <input_file>")

  if not os.path.isfile(sys.argv[1]):
    raise FileNotFoundError("File {} does not exist!".format(sys.argv[1]))

  input_file = sys.argv[1]

  lines = []
  with open(input_file, 'r') as infile:
    lines = infile.readlines()
  
  # Generate a matrix of all the digits so we can use numpy to do some easy sums
  # along the column axis.
  num_lines = len(lines)
  num_digits = len(lines[0].strip())
  matrix = np.zeros((num_lines, num_digits), dtype=np.int64)

  # Fill in the matrix with the binary digits.
  for i in range(0, len(lines)):
    matrix[i, :] = str_to_digits(lines[i].strip())
    
  # Calculate the sum along columns. We can compare each sum to (num_lines/2) to
  # get the most common digit. If sum > (num_lines/2), then 1 is the most common
  # digit, else 0 is.
  sums = matrix.sum(axis=0)
  epsilon_arr = np.where(sums > (num_lines / 2), 1, 0)
  gamma_arr = np.where(sums > (num_lines / 2), 0, 1)

  # Now convert the arrays of digits into actual binary numbers.
  epsilon = digits_to_num(epsilon_arr)
  gamma = digits_to_num(gamma_arr)
  pwr = epsilon * gamma

  print("Epsilon: {}\nGamma: {}\nPower consumption: {}".format(
    epsilon, gamma, pwr))

  oxygen_arr = np.squeeze(reduce_bit_criteria_most_common(matrix, 0))
  co2_arr = np.squeeze(reduce_bit_criteria_least_common(matrix, 0))

  o2_gen = digits_to_num(oxygen_arr)
  co2_scrub = digits_to_num(co2_arr)
  life_support = o2_gen * co2_scrub

  print("\nO2 Generator: {}\nCO2 Scrubber: {}\nLife Support: {}".format(
    o2_gen, co2_scrub, life_support))
