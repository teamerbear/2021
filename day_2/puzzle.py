import os, sys


# The final position for test_1.txt using the first method should be horiz: 15,
# depth: 10 (multiplied 150)
# The final position for test_2.txt using the first method should be horiz: 12,
# depth: 25 (multiplied 300)

# The final position for test_1.txt using the second method should be horiz: 15,
# depth: 60 (multiplied 900)
# The final position for test_2.txt using the second method should be horiz: 12,
# depth: 219 (multiplied 2628)

# The final position for input.txt using the second method should be
# horiz: 2010, depth: 1030 (multiplied 2070300)
# The final position for input.txt using the second method should be
# horiz: 2010, depth: 1034321 (multiplied 2078985210)


def check_commands_valid(commands):
  command_set = set()
  for command in commands:
    command_set.add(command.strip().split()[0].lower())
  
  if len(command_set) != 3:
    raise ValueError(
      "The length of the command set should be 3, but isn't! ({})".format(
        len(command_set)))
  
  for command in command_set:
    if command != "up" and command != "down" and command != "forward":
      raise ValueError("Found an invalid command: {}".format(command))


def process_simple_command(command):
  pos_delta = [0, 0]
  parts = command.strip().split()
  direction = parts[0].lower()[0]
  num = int(parts[1])
  if direction == 'f':
    pos_delta[0] += num
  elif direction == 'd':
    pos_delta[1] += num
  elif direction == 'u':
    pos_delta[1] -= num
  else:
    raise ValueError("Found invalid direction: \'{}\'!".format(direction))
  
  return pos_delta

def process_command(command, curr_aim):
  pos_delta = [0, 0, 0] # horiz, depth, aim
  parts = command.strip().split()
  direction = parts[0].lower()[0]
  num = int(parts[1])
  if direction == 'f':
    pos_delta[0] += num
    pos_delta[1] += (num * curr_aim)
  elif direction == 'd':
    pos_delta[2] += num
  elif direction == 'u':
    pos_delta[2] -= num
  else:
    raise ValueError("Found invalid direction: \'{}\'!".format(direction))
  
  return pos_delta


if __name__ == "__main__":
  if len(sys.argv) < 2:
    raise SyntaxError("USAGE: python3 puzzle.py <input_file>")

  if not os.path.isfile(sys.argv[1]):
    raise FileNotFoundError("File {} does not exist!".format(sys.argv[1]))

  input_file = sys.argv[1]

  commands = []
  with open(input_file, 'r') as infile:
    lines = infile.readlines()
    commands = [line.strip() for line in lines]
  
  check_commands_valid(commands)

  curr_horiz = 0
  curr_depth = 0
  for command in commands:
    delta = process_simple_command(command)
    curr_horiz += delta[0]
    curr_depth += delta[1]
  
  print("Final position (simple): horiz {}, depth {}".format(curr_horiz, curr_depth))
  print("Multiplication of position components: {}".format(curr_horiz * curr_depth))

  curr_horiz = 0
  curr_depth = 0
  curr_aim = 0
  for command in commands:
    delta = process_command(command, curr_aim)
    curr_horiz += delta[0]
    curr_depth += delta[1]
    curr_aim += delta[2]
  
  print("\nFinal position (complex): horiz {}, depth {}".format(curr_horiz, curr_depth))
  print("Multiplication of position components: {}".format(curr_horiz * curr_depth))
