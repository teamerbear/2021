import os, sys
import math
import numpy as np


# For test_1.txt:
#   The number of coordinates that are covered by two or more horizontal or
#   vertical lines is 5.
#   The number of coordinates that are covered by horizontal, vertical and
#   diagonal lines is 12.
#
# For test_2.txt:
#   The number of coordinates that are covered by two or more horizontal or
#   vertical lines is 6.
#   The number of coordinates that are covered by horizontal, vertical and
#   diagonal lines is 10.
#
# For input.txt:
#   The number of coordinates that are covered by two or more horizontal or
#   vertical lines is 3990.
#   The number of coordinates that are covered by horizontal, vertical and
#   diagonal lines is 21305.
#


class Line(object):
  def __init__(self, x1, y1, x2, y2):
    self.x1 = x1
    self.y1 = y1
    self.x2 = x2
    self.y2 = y2
  
  def is_horizontal(self):
    return self.y1 == self.y2
  
  def is_vertical(self):
    return self.x1 == self.x2
  
  def is_diagonal(self):
    return abs(self.x1 - self.x2) == abs(self.y1 - self.y2)


def parse_input_file(input_file):
  lines = []
  with open(input_file, 'r') as infile:
    lines = infile.readlines()

  vent_lines = []
  max_x = 0
  max_y = 0
  for line in lines:
    coords = [int(x.strip()) for x in line.strip().replace(' -> ', ',').split(',')]
    if len(coords) != 4:
      raise ValueError("Each line should have four coordinate values!")
    
    vent_lines.append(Line(coords[0], coords[1], coords[2], coords[3]))
    if coords[0] > max_x:
      max_x = coords[0]
    if coords[2] > max_x:
      max_x = coords[2]
    if coords[1] > max_y:
      max_y = coords[1]
    if coords[3] > max_y:
      max_y = coords[3]

  return vent_lines, max_x, max_y
  

def mark_horizontal(line, grid):
  begin = 0
  end = 0
  if line.x1 < line.x2:
    begin = line.x1
    end = line.x2 + 1
  else:
    begin = line.x2
    end = line.x1 + 1

  for i in range(begin, end):
    grid[i, line.y1] += 1


def mark_vertical(line, grid):
  begin = 0
  end = 0
  if line.y1 < line.y2:
    begin = line.y1
    end = line.y2 + 1
  else:
    begin = line.y2
    end = line.y1 + 1
  
  for i in range(begin, end):
    grid[line.x1, i] += 1


def mark_diagonal(line, grid):
  x_coords = []
  y_coords = []
  if line.x1 > line.x2:
    x_coords = list(range(line.x2, line.x1 + 1))
    x_coords.reverse()
  else:
    x_coords = list(range(line.x1, line.x2 + 1))
  
  if line.y1 > line.y2:
    y_coords = list(range(line.y2, line.y1 + 1))
    y_coords.reverse()
  else:
    y_coords = list(range(line.y1, line.y2 + 1))
  
  for i in range(len(x_coords)):
    grid[x_coords[i], y_coords[i]] += 1


if __name__ == "__main__":
  if len(sys.argv) < 2:
    raise SyntaxError("USAGE: python3 puzzle.py <input_file>")

  if not os.path.isfile(sys.argv[1]):
    raise FileNotFoundError("File {} does not exist!".format(sys.argv[1]))

  input_file = sys.argv[1]
  vent_lines, max_x, max_y = parse_input_file(input_file)

  # Initialize a grid of zeros.
  grid = np.zeros((max_x + 1, max_y + 1), dtype=np.int64)

  # First, consider only horizontal and vertical lines.
  for line in vent_lines:
    if line.is_horizontal():
      mark_horizontal(line, grid)

    elif line.is_vertical():
      mark_vertical(line, grid)

  # Now find any coordinate where the number of lines crossing is more than 2.
  x_coords, y_coords = np.where(grid >= 2)
  print("Number of points where two lines intersect: {}".format(len(x_coords)))

  # Now do the same thing, but include diagonal lines.
  grid = np.zeros((max_x + 1, max_y + 1), dtype=np.int64)
  for line in vent_lines:
    if line.is_horizontal():
      mark_horizontal(line, grid)

    elif line.is_vertical():
      mark_vertical(line, grid)
    
    elif line.is_diagonal():
      mark_diagonal(line, grid)

  # Now find any coordinate where the number of lines crossing is more than 2.
  x_coords, y_coords = np.where(grid >= 2)
  print("Number of points where two lines intersect (considering diagonals): {}".format(len(x_coords)))