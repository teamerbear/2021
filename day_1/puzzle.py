import math
import os, sys


# The test_1.txt file should have 10 increasing single measurements.
# The test_2.txt file should have 32 increasing single measurements.

# The test_1.txt file should have 9 increasing sliding windows.
# The test_2.txt file should have 30 increasing sliding windows.

# For the real input file, I calculated 1292 increasing single measurements.
# For the real input file, I calculated 1262 increasing sliding windows.


def count_increasing(x):
  num_increasing = 0
  for i in range(0, len(x) - 1):
    if x[i + 1] > x[i]:
      num_increasing += 1

  return num_increasing


if __name__ == "__main__":
  if len(sys.argv) < 2:
    raise SyntaxError("USAGE: python3 puzzle.py <input_file>")

  if not os.path.isfile(sys.argv[1]):
    raise FileNotFoundError("File {} does not exist!".format(sys.argv[1]))

  input_file = sys.argv[1]
  lines = []
  with open(input_file, 'r') as infile:
    lines = infile.readlines()
  
  depths = [int(x.strip().split()[0]) for x in lines]
  windows = []
  for i in range(0, len(depths) - 2):
    windows.append(depths[i] + depths[i + 1] + depths[i + 2])

  num_incr_depths = count_increasing(depths)
  num_incr_windows = count_increasing(windows)

  print("There are {} increasing single depth measurements!".format(
    num_incr_depths))
  print("There are {} increasing sliding windows!".format(num_incr_windows))