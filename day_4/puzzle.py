import os, sys
import numpy as np


# For test_1.txt:
#   Board 3 is the winner
#   Sum of unmarked numbers is 188
#   Winning number is 24
#   Final score is 4512
# 
#   Board 2 is the loser
#   Sum of unmarked numbers is 148
#   Winning number is 13
#   Final Score is 1924
#
# For test_2.txt:
#   Board 3 is the winner
#   Sum of unmarked numbers is 189
#   Winning number is 24
#   Final score is 4536
#
#   Board 2 is the loser
#   Sum of unmarked numbers is 148
#   Winning number is 13
#   Final Score is 1924
#
# For input.txt:
#   Board 3 is the winner
#   Sum of unmarked numbers is 189
#   Winning number is 24
#   Final score is 4536

#   Board 66 is the loser
#   Sum of unmarked numbers is 246
#   Winning number is 20
#   Final Score is 4920
#

class BingoBoard(object):
  def __init__(self, nums):
    if nums.shape != (5, 5):
      raise ValueError("The bingo board must be a 5 x 5 grid!")

    self._nums = nums
    # Unmarked = 1, Marked = 0
    # This way, we can just multiply _marked by _nums and sum to get the sum of
    # the unmarked values.
    self._marked = np.ones((5, 5), dtype=np.bool8)
    self.last_num_called = -1

  def mark(self, num):
    self.last_num_called = num

    # Find where the value is.
    coords_full = np.where(self._nums == num)
    if coords_full[0].shape[0] > 1 or coords_full[1].shape[0] > 1:
      raise ValueError("The bingo board is invalid (multiple entries the same)!")
    
    if coords_full[0].shape[0] == 1 and coords_full[1].shape[0] == 1:
      self._marked[coords_full[0][0], coords_full[1][0]] = 0

  def check_bingo(self):
    # Check rows and columns.
    col_sums = np.sum(self._marked, axis=0)
    row_sums = np.sum(self._marked, axis=1)
    for s in col_sums:
      if s == 0:
        return True
    for s in row_sums:
      if s == 0:
        return True
    
    # If no bingos were found . . .
    return False
      
  def sum_unmarked(self):
    mul = np.multiply(self._nums, self._marked, dtype=np.int64)
    return np.sum(mul, dtype=np.int64)
  
  def get_final_score(self):
    return self.sum_unmarked() * self.last_num_called


def parse_board_from_lines(board_lines):
  if len(board_lines) != 5:
    raise ValueError("Need five lines to parse out a board!")
  
  board_nums = np.zeros((5, 5), dtype=np.int64)

  for i in range(0, len(board_lines)):
    line_nums = [int(x.strip()) for x in board_lines[i].strip().split()]
    if len(line_nums) != 5:
      raise ValueError("Need five numbers to parse out a board row!")
    
    board_nums[i, :] = line_nums
  
  board = BingoBoard(board_nums)
  return board


def parse_input_file(input_file):
  lines = []
  with open(input_file, 'r') as infile:
    lines = infile.readlines()

  called_nums = [int(x.strip()) for x in lines[0].strip().split(',')]
  boards = []

  curr_lines = []
  for line in lines[1:]:
    if line.strip() != "":
      curr_lines.append(line)
      if len(curr_lines) == 5:
        boards.append(parse_board_from_lines(curr_lines))
        curr_lines = []

  return called_nums, boards
  


if __name__ == "__main__":
  if len(sys.argv) < 2:
    raise SyntaxError("USAGE: python3 puzzle.py <input_file>")

  if not os.path.isfile(sys.argv[1]):
    raise FileNotFoundError("File {} does not exist!".format(sys.argv[1]))

  input_file = sys.argv[1]
  called_nums, boards = parse_input_file(input_file)

  winners = np.zeros((len(boards),), dtype=np.bool8)
  loser_board = None
  loser_ind = -1
  found_first_winner = False
  while len(called_nums) > 0:
    for i in range(0, len(boards)):
      if winners[i] != 1:
        boards[i].mark(called_nums[0])
        won = boards[i].check_bingo()
        if won:
          winners[i] = 1
          if not found_first_winner:
            found_first_winner = True
            final_score = boards[i].get_final_score()
            print("Winner: Board {}!".format(i + 1))
            print("\tWinning number: {}".format(boards[i].last_num_called))
            print("\tSum of unmarked: {}".format(boards[i].sum_unmarked()))
            print("\tFinal Score: {}".format(boards[i].get_final_score()))
          # We don't do a break here so we can see if multiple boards won at the same time.
    
    called_nums.remove(called_nums[0])

    if np.sum(winners) == len(winners) - 1:
      loser_ind = np.where(winners == 0)[0][0]
      loser_board = boards[loser_ind]
      break
  
  last_win = False
  while len(called_nums) > 0:
    loser_board.mark(called_nums[0])
    called_nums.remove(called_nums[0])
    last_win = loser_board.check_bingo()
    if last_win:
      print("Loser: Board {}!".format(loser_ind + 1))
      print("\tWinning number: {}".format(loser_board.last_num_called))
      print("\tSum of unmarked: {}".format(loser_board.sum_unmarked()))
      print("\tFinal Score: {}".format(loser_board.get_final_score()))
      break;

